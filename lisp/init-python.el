;;; init-python.el --- Python-specific Emacs configuration -* lexical-binding: t -*-

;; group
(defgroup boa
  nil
  "Python utilities."
  :group 'convenience)

;; boa variables
(defcustom boa-clean-all-virtualenvs
  t
  "Clean all virtual environments from \"exec-path\"
when deactivating."
  :type 'boolean
  :group 'boa)

(defcustom boa-formatter
  "yapf"
  "The default Python formatter."
  :type 'string
  :group 'boa)

(defcustom boa-install-extra-packages-p
  t
  "If t, install extra packages when creating a virtual environment.
The extra packages are pyflakes, pylint, and yapf."
  :type 'boolean
  :group 'boa)

(defcustom boa-default-test-command
  "pytest"
  "The default test command."
  :type 'string
  :group 'boa)

(defvar boa-current-virtualenv
  nil
  "The current Python virtual environment.")

;; other variables
(defvar anaconda-mode-localhost-address)
(defvar python-shell-exec-path)
(defvar python-shell-virtualenv-root)

;; anaconda-mode
(with-eval-after-load 'anaconda-mode
  (setq anaconda-mode-localhost-address "127.0.0.1"))

;; functions
(defun boa--clean-up-exec-path (virtualenv)
  "Removes VIRTUALENV from \"exec-path\".
If boa-clean-all-virtualenvs is t, removes all virtual environments
from \"exec-path\"."
  (if boa-clean-all-virtualenvs
      (setq exec-path (seq-filter (lambda (element)
                                    (not (string-match-p
                                          "/virtualenvs/" element)))
                                  exec-path))
    (setq exec-path (seq-filter (lambda (element)
                                  (not (string-match-p
                                        virtualenv element))
                                  exec-path)))))

(defun boa-format-buffer (buffer &optional formatter)
  "Formats BUFFER with FORMATTER.
The default formatter is pylint."
  (interactive "bBuffer: ")
  (if (not formatter)
      (setq formatter boa-formatter))
  (let ((point-pos (point)))
    (with-current-buffer (get-buffer buffer)
      (call-process-region (point-min) (point-max)
                           boa-formatter t t t))
    (goto-char point-pos))
  (message "Formatted buffer"))

(defun boa-format-current-buffer ()
  "Formats the current buffer."
  (interactive)
  (boa-format-buffer (current-buffer)))

(defun boa-test ()
  "Runs the default test command in the project root."
  (interactive)
  (let ((default-directory (cdr (project-current)))
        (test-buffer "*boa: test*"))
    (start-process "pytest" (get-buffer-create "*boa: test*")
                   boa-default-test-command)))

(defun boa-venv-activate (virtualenv)
  "Activates the Python virtual environment, VIRTUALENV."
  (interactive "DEnter the path to the virtual environment: ")
  (if (not (null boa-current-virtualenv))
      (error "A virtual environment is already active (%s)"
             boa-current-virtualenv))
  (dolist (the-list (list 'exec-path 'python-shell-exec-path))
    (add-to-list the-list (format "%s/bin" virtualenv)))
  (setq boa-current-virtualenv virtualenv
        python-shell-virtualenv-root virtualenv)
  (if (bound-and-true-p flymake-mode)
      (progn
        (flymake-mode -1)
        (flymake-mode 1)))
  (message "Activated virtual environment (%s); to deactivate,
use boa-venv-deactivate" virtualenv))

(defun boa-venv-deactivate ()
  "Deactivates the active Python virtual environment,
if there is one."
  (interactive)
  (if (and (called-interactively-p "any")
           (null boa-current-virtualenv))
      (error "No active virtual environment"))
  (when boa-current-virtualenv
    (let ((parting-message (format
                            "Deactivated virtual environment %s"
                            boa-current-virtualenv)))
      (boa--clean-up-exec-path boa-current-virtualenv)
      (setq boa-current-virtualenv nil
            python-shell-exec-path nil
            python-shell-virtualenv-root nil)
      (message "%s" parting-message))))

(defun boa--venv-prompt-activate (virtualenv)
  "Prompts to activate VIRTUALENV."
  (when (y-or-n-p (format
                   "Found virtual environment (%s) -- Activate? "
                   virtualenv))
    (boa-venv-activate virtualenv)))

(defun boa--pipenv-prompt-create ()
  "Prompts to create a pipenv virtual environment
in current project directory."
  (if (y-or-n-p "Create a virtual environment? ")
      (progn
        (message "Creating virtual environment with pipenv...")
        (if boa-install-extra-packages-p
            (call-process "pipenv" nil (get-buffer-create "*pipenv*")
                          nil "install" "pyflakes" "pylint" "yapf")
          (call-process "pipenv" nil (get-buffer-create "*pipenv*")
                        nil "install"))
        (boa-venv-activate (string-trim-right
                            (shell-command-to-string
                             "pipenv --venv"))))))

(defun boa-pipenv-prompt ()
  "Offers to activate pipenv in project."
  (interactive)
  (if (and (called-interactively-p "any") boa-current-virtualenv)
      (error "Virtual environment already active (%s)"
             boa-current-virtualenv))
  (unless boa-current-virtualenv
    (let* ((current-project (cdr (project-current)))
           (pipfile-exists-p (file-exists-p
                              (format "%s%s"
                                      current-project "Pipfile")))
           (virtualenv-root (string-trim-right
                             (shell-command-to-string
                              "pipenv --venv"))))
      (cond ((and (called-interactively-p "any")
                  (not pipfile-exists-p))
             (error "Could not locate virtual environment"))
            ((and pipfile-exists-p
                  (string-match-p "No virtualenv has been created"
                                  virtualenv-root))
             (boa--pipenv-prompt-create))
            (t (boa--venv-prompt-activate virtualenv-root))))))

(defun boa-pylint (buffer)
  "Sends BUFFER to pylint and displays the result."
  (interactive "bBuffer: ")
  (let ((lint-buffer "*boa: lint*"))
    (with-current-buffer (get-buffer-create lint-buffer)
      (view-mode -1)
      (read-only-mode -1)
      (erase-buffer))
    (call-process-region (point-min) (point-max) "pylint" nil
                         (get-buffer lint-buffer) t "--from-stdin"
                         (file-name-base (buffer-file-name buffer)))
    (with-current-buffer (get-buffer lint-buffer)
      (view-mode t))
    (display-buffer lint-buffer 'display-buffer-use-some-window
                    'inhibit-same-window))
  (message "Linted buffer"))

(defun boa-pylint-current-buffer ()
  "Sends the contents of current buffer to pylint."
  (interactive)
  (boa-pylint (current-buffer)))

(define-minor-mode boa-mode
  "Toggles \"boa-mode\"."
  :init-value nil
  :lighter " Boa"
  :keymap (let ((map (make-sparse-keymap)))
            (define-key map (kbd "C-c f") 'boa-format-current-buffer)
            (define-key map (kbd "C-c p") 'boa-pipenv-prompt)
            (define-key map (kbd "C-c l") 'boa-pylint-current-buffer)
            (define-key map (kbd "C-c t") 'boa-test)
            (define-key map (kbd "C-c v") 'boa-venv-activate)
            (define-key map (kbd "C-c d") 'boa-venv-deactivate)
            map)
  (boa-pipenv-prompt))

;; mode hooks
(dolist (mode (list #'boa-mode #'anaconda-mode #'anaconda-eldoc-mode))
  (add-hook 'python-mode-hook mode))

(provide 'init-python)
;;; init-python.el ends here
