;;; init-packages.el --- Package configuration -*- lexical-binding: t -*-

;; Author: Scott Myers
;; Maintainer: Scott Myers
;; Version: 0.1.0
;; Package-Requires: ()
;; Homepage: N/A
;; Keywords: N/A


;; This file is not part of GNU Emacs

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; Package setup

;;; Code:

(require 'package)

;; install these packages
(defvar package-list '(aggressive-indent
                       anaconda-mode
                       exec-path-from-shell
                       go-mode
                       json-mode
                       lsp-mode
                       lsp-ui
                       magit
                       marginalia
                       orderless
                       paredit
                       project
                       slime
                       vertico
                       which-key
                       yaml-mode
                       yasnippet
                       yasnippet-snippets))

(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))

(unless package-archive-contents
  (package-refresh-contents))

(dolist (package package-list)
  (unless (package-installed-p package)
    (package-install package)))

;; marginalia
(require 'marginalia)

;; orderless
(require 'orderless)
(setq completion-styles '(orderless)
      completion-category-defaults nil
      completion-category-overrides '((file
                                       (styles partial-completion))))

;; project
(require 'project)

;; uniquify
(require 'uniquify)

;; vertico
(require 'vertico)
(vertico-mode +1)

;; which-key
(require 'which-key)
(which-key-mode +1)

;; yasnippet
(require 'yasnippet)
(yas-global-mode +1)

(provide 'init-packages)

;;; init-packages.el ends here
