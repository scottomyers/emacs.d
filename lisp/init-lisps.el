;;; init-lisps.el --- Lisp configuration -*- lexical-binding: t -*-

;; Author: Scott Myers
;; Maintainer: Scott Myers
;; Version: 0.1.0
;; Package-Requires: ()
;; Homepage: N/A
;; Keywords: N/A


;; This file is not part of GNU Emacs

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; Lisp configs

;;; Code:
(require 'paredit nil t)

(setq paredit-lighter " ()")

(global-set-key (kbd "C-c s") 'slime-selector)

(setq inferior-lisp-program "sbcl"
      slime-net-coding-system 'utf-8-unix)

(dolist (hook (list 'emacs-lisp-mode-hook 'slime-mode-hook))
  (add-hook hook #'aggressive-indent-mode))

(dolist (hook (list 'emacs-lisp-mode-hook 'lisp-interaction-mode-hook
                    'lisp-mode-hook 'slime-mode-hook))
  (add-hook hook #'enable-paredit-mode))

(provide 'init-lisps)

;;; init-lisps.el ends here
