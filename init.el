;;; init.el --- Emacs configuration -*- lexical-binding: t -*-

;;; Commentary: My Emacs config

;;; Code:

(let ((minver "27.0"))
  (when (version< emacs-version minver)
    (error "Emacs is out of date -- config requires v%s or higher"
           minver)))

(add-to-list 'load-path
             (expand-file-name "lisp" user-emacs-directory))

(require 'init-packages)
(require 'init-defaults)
(require 'init-flymake)
(require 'init-lisps)
(require 'init-lsp)
(require 'init-org)
(require 'init-python)

;; customization -- override previous settings
(setq custom-file (locate-user-emacs-file "custom.el"))
(when (file-exists-p custom-file)
  (load custom-file))

;; locales -- these should be set toward the end of the file
(setenv "LANG" "en_US.UTF-8")
(setenv "LC_ALL" "en_US.UTF-8")
(setenv "LC_CTYPE" "en_US.UTF-8")

(provide 'init)
;;; init.el ends here
