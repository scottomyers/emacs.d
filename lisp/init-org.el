;;; init-org.el --- org-mode config -*- lexical-binding: t -*-

;; Author: Scott Myers
;; Maintainer: Scott Myers
;; Version: 0.1.0
;; Package-Requires: ()
;; Homepage: N/A
;; Keywords: N/A


;; This file is not part of GNU Emacs

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; Config for org-mode

;;; Code:

;; org
(require 'org)

(setq org-adapt-indentation nil
      org-default-notes-file "~/notes/todo.org"
      org-edit-src-content-indentation 0)

;; capture templates
(defvar org-capture-templates)
(setq org-capture-templates '(("t" "Todo" entry (file+headline "~/notes/todo.org" "Backlog") "* TODO %?\n%T\n%a" :prepend t)
                              ("j" "Journal" entry (file "~/notes/journal.org") "* %T\n%?" :prepend t)))

;; keybinds
(global-set-key (kbd "C-c l") #'org-store-link)
(global-set-key (kbd "C-c a") #'org-agenda)
(global-set-key (kbd "C-c c") #'org-capture)

(provide 'init-org)

;;; init-org.el ends here
