;;; init-flymake.el --- Flymake-specific config -*- lexical-binding: t -*-

;; Author: Scott Myers
;; Maintainer: Scott Myers
;; Version: 0.1.0
;; Package-Requires: ()
;; Homepage: N/A
;; Keywords: N/A


;; This file is not part of GNU Emacs

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; Flymake configuration

;;; Code:

;; flymake
(require 'flymake)

(setq-default flymake-no-changes-timeout '2)

(define-key flymake-mode-map (kbd "M-n") 'flymake-goto-next-error)

(define-key flymake-mode-map (kbd "M-p") 'flymake-goto-prev-error)

(define-key flymake-mode-map (kbd "C-c e")
            'flymake-show-diagnostics-buffer)

(add-hook 'emacs-lisp-mode-hook #'flymake-mode 1)

(add-hook 'python-mode-hook (lambda ()
                              (unless (eq buffer-file-name nil)
                                (flymake-mode 1))))

(provide 'init-flymake)

;;; init-flymake.el ends here
