;;; init-defaults.el --- Default / generic configuration -*- lexical-binding: t -*-

;; Author: Scott Myers
;; Maintainer: Scott Myers
;; Version: 0.1.0
;; Package-Requires: ()
;; Homepage: N/A
;; Keywords: N/A


;; This file is not part of GNU Emacs

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; Generic Emacs configuration

;;; Code:

;; garbage collection
(let ((normal-gc-cons-threshold (* 20 1024 1024))
      (init-gc-cons-threshold (* 128 1024 1024)))
  (setq gc-cons-threshold init-gc-cons-threshold)
  (add-hook 'emacs-startup-hook
            (lambda ()
              (setq gc-cons-threshold normal-gc-cons-threshold))))

;; backups
(make-directory "~/.emacs.d/backups" t)

(setq backup-directory-alist `(("." . "~/.emacs.d/backups"))
      delete-old-versions t
      kept-new-versions 6
      kept-old-versions 2
      version-control t)

;; PATH
(when (and (fboundp 'exec-path-from-shell-initialize)
           (or (daemonp)
               (memq window-system '(mac ns x))))
  (exec-path-from-shell-initialize))

;; defaults
(column-number-mode t)

(fset 'yes-or-no-p 'y-or-n-p)

(global-set-key (kbd "C-x C-b") 'ibuffer)

(global-unset-key (kbd "C-z"))

(menu-bar-mode -1)

(prefer-coding-system 'utf-8)

(scroll-bar-mode -1)

(set-default-coding-systems 'utf-8)

(set-terminal-coding-system 'utf-8)

(set-keyboard-coding-system 'utf-8)

(setq compilation-ask-about-save nil
      custom-safe-themes t
      inhibit-splash-screen t
      load-prefer-newer t
      read-process-output-max (* 1024 1024)
      ring-bell-function 'ignore)


(setq-default fill-column 80
              indent-tabs-mode nil
              search-upper-case 'not-yanks
              size-indication-mode t
              tab-width 8
              truncate-lines t)

(show-paren-mode t)

(tool-bar-mode -1)

;; mouse
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))
      mouse-wheel-progressive-speed nil
      mouse-wheel-follow-mouse t)

(when (eq window-system nil)
  (xterm-mouse-mode t))

;; themes
(defvar sm/preferred-theme 'modus-operandi)

(defun sm/load-preferred-theme ()
  "Load \"SM/PREFERRED-THEME\" if it is available."
  (when (member sm/preferred-theme (custom-available-themes))
    (load-theme sm/preferred-theme)))

(if (display-graphic-p)
    (sm/load-preferred-theme))

;; window management
(when (fboundp 'windmove-default-keybindings)
  (windmove-default-keybindings))

;;; full screen startup
(if 'toggle-frame-maximized
    (toggle-frame-maximized))

(provide 'init-defaults)

;;; init-defaults.el ends here
